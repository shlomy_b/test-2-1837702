/**
 * HourlyEmployee implements Employee
 * 
 * @author Shlomo Bensimhon
 * @since	2020-11-10
 *
 */
public class HourlyEmployee implements Employee {
	private double hoursWorked;
	private double hourlyPay;
	
	public HourlyEmployee(double hoursWorked, double hourlyPay) {
		this.hoursWorked = hoursWorked;
		this.hourlyPay = hourlyPay;
	}
	
	public double getHoursWorked() {
		return this.hoursWorked;
	}
	
	public double getHourlyPay() {
		return this.hourlyPay;
	}
	
	public double getWeeklyPay() {
		return (this.hoursWorked * this.hourlyPay);
	}
}
