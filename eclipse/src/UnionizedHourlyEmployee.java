/**
 * UnionizedHourlyEmployee extends HourlyEmployee
 * 
 * @author Shlomo Bensimhon
 * @since	2020-11-10
 *
 */
public class UnionizedHourlyEmployee extends HourlyEmployee{
	private double maxHoursPerWeek;
	private double overtimeRate;
	
	public UnionizedHourlyEmployee(double hoursWorked, double hourlyPay, double maxHoursPerWeek, double overtimeRate) {
		super(hoursWorked, hourlyPay);
		this.maxHoursPerWeek = maxHoursPerWeek;
		this.overtimeRate = overtimeRate;
	}
	
	public double getMaxHoursPerWeek() {
		return this.maxHoursPerWeek;
	}
	
	public double getOvertimeRate() {
		return this.overtimeRate;
	}
	
	public double getWeeklyPay() {
		double paycheck = 0;
		
		if(getHoursWorked() <= this.maxHoursPerWeek) {
			paycheck = (this.getHoursWorked() * this.getHourlyPay());
		}
		if(getHoursWorked() >= this.maxHoursPerWeek) {
			double overtime = (this.getHoursWorked() - this.maxHoursPerWeek);
			paycheck =  ((overtime * this.overtimeRate) + (this.maxHoursPerWeek * this.getHourlyPay()));
		}
		return paycheck;
	}
}








