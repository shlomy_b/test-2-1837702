package question;

/*
 * A class to store information about a planet
 * @author Daniel
 *
 */
public class Planet implements Comparable <Planet>{
	/**
	 * The name of the solar system the planet is contained within
	 */
	private String planetarySystemName;
	
	/**
	 * The name of the planet.
	 */
	private String name;
	
	/**
	 * From inside to outside, what order is the planet. (e.g. Mercury = 1, Venus = 2, etc)
	 */
	private int order;
	
	/**
	 * The size of the radius in Kilometers.
	 */
	private double radius;

	/**
	 * A constructor that initializes the Planet object
	 * @param planetarySystemName The name of the system that the planet is a part of
	 * @param name The name of the planet
	 * @param order The order from inside to out of how close to the center the planet is
	 * @param radius The radius of the planet in kilometers
	 */
	public Planet(String planetarySystemName, String name, int order, double radius) {
		this.planetarySystemName = planetarySystemName;
		this.name = name;
		this.order = order;
		this.radius = radius;
	}
	
	/**
	 * @return The name of the planetary system (e.g. "the solar system")
	 */
	public String getPlanetarySystemName() {
		return planetarySystemName;
	}
	
	/**
	 * @return The name of the planet (e.g. Earth or Venus)
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @return The rank of the planetary system
	 */
	public int getOrder() {
		return order;
	}


	/**
	 * @return The radius of the planet in question.
	 */
	public double getRadius() {
		return radius;
	}
	
	public boolean equals(Object obj) {
		Planet cast = (Planet) obj;
		if(cast.name.equals(this.name) && cast.planetarySystemName.equals(this.planetarySystemName)) {
			return true;
		}
		return false;
	}
	
	public int hashCode() {
		String combined = (this.name + this.planetarySystemName);
		return combined.hashCode();
	}

	public int compareTo(Planet obj) {
	int stringCompare = this.getName().compareTo(obj.getName());
	
	if(stringCompare == 0) {
		if ((this.getRadius() - obj.getRadius()) == 0) {
			return 0;
		}
		if ((this.getRadius() - obj.getRadius()) < 0) {
			return -1;
		}else{
			return 1;
		}
	}
		return stringCompare;
	}
}






