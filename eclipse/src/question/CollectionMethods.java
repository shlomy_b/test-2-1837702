/**
 * CollectionMethods is a class with a method 
 * 
 * @author Shlomo Bensimhon
 * @since	2020-11-10
 *
 */
package question;
import java.util.*;

public class CollectionMethods {
	
	public static Collection<Planet> getLargerThan(Collection<Planet> planets, double size){

		Collection<Planet> tempPlanets = new ArrayList<Planet>(planets);
		//Planet[] planetArray = new Planet[planets.size()];
		
 		for(int i = 0; i < planets.size(); i++) {
 			if(((ArrayList<Planet>)planets).get(i).getRadius() >= size) {
 				//tempPlanets.add((ArrayList<Planet>)planets).get(i);
 				planets.remove(((ArrayList<Planet>)tempPlanets).get(i));
 			}
		}
 		return tempPlanets; 
	}
}
