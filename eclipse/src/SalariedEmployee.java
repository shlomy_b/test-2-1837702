public class SalariedEmployee implements Employee{
	
	private double yearly;
	
	public SalariedEmployee(double yearly) {
		this.yearly = yearly;
	}
	
	
	
	public double getYearly() {
		return this.yearly;
	}
	
	public double getWeeklyPay(){
		return (this.yearly / 52);
	}
	
	
}
