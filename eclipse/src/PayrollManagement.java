/**
 * PayrollManagement is a class with a main method 
 * 
 * @author Shlomo Bensimhon
 * @since	2020-11-10
 *
 */
public class PayrollManagement {
	public static void main(String[] args) {
		
		Employee[] emps1 = new Employee[5]; 
		emps1[0] = new HourlyEmployee(30, 12.00);
		emps1[1] = new HourlyEmployee(35, 12.00); 
		emps1[2] = new SalariedEmployee(130000.00); 
		emps1[3] = new SalariedEmployee(60000.00); 
		emps1[4] = new UnionizedHourlyEmployee(25, 13, 40, 1.5);
		
		System.out.println(emps1[4].getWeeklyPay());
		System.out.println(getTotalExpenses(emps1));
	}
	
	public static double getTotalExpenses(Employee[] employees){
		double totalExpense = 0;
		
		for(int i = 0; i < employees.length; i++) {
			totalExpense += employees[i].getWeeklyPay();
		}
		return totalExpense;
	}
}
